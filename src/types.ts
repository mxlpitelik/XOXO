export enum XOXO { empty = '', x = 'x', o = 'o' }

export interface XOXOArray {
    [index: number]: XOXO;
}

export interface GameData {
  size: number;
  x: boolean;
  count: number;
  finish: boolean;
  data: XOXOArray;
  versus: boolean;
}

export interface CellData {
  position: number;
  value: XOXO;
}

export type CellDataGetter = (position: number) => XOXO;
