import {GetterTree, MutationTree, ActionTree, ActionContext} from 'Vuex';
import {CellData, GameData, XOXO, XOXOArray, CellDataGetter} from '@/types';
import {State} from './State';

// вместо прямого доступа к стэйтам я рекуомендую всегда обращаться через геттеры
// так как тогда мы повышаем абстракцию и не привязаны к структуре стэйта в компонентах

export const getters = {
  symbol(state): XOXO {
    return (state.x) ? XOXO.x : XOXO.o;
  },
  count(state): number {
    return state.count;
  },

  // тут мы возвращаем функцию и поэтому добавил слово гет
  // так как она подсказывает что мы ожидаем параметр

  getCellData: (state): CellDataGetter => (position) => {
    return (state.data[position]) ? state.data[position] : XOXO.empty;
  },
  gameData(state): XOXOArray {
    return state.data;
  },
  gameOver(state): boolean {
    return state.finish;
  },
  size(state): number {
    return state.size || 3;
  },
  versus(state): boolean {
    return state.versus;
  },
} as GetterTree<State, any>;


// из-за того что мы определили все типы в интерфесе -
// то нам нет необходимости указывать их явно

// export const getters = {
//   getSymbol(state: State): XOXO {
//     return (state.x) ? XOXO.x : XOXO.o;
//   },
//   getCount(state: State): number {
//     return state.count;
//   },
//   getCellData: (state: State): CellDataGetter => (position: number): XOXO => {
//     return (state.data[position]) ? state.data[position] : XOXO.empty;
//   },
//   getGameData(state: State): XOXOArray {
//     return state.data;
//   },
//   isGameOver(state: State): boolean {
//     return state.finish;
//   },
//   getSize(state: State): number {
//     return state.size || 3;
//   },
//   isVersus(state: State): boolean {
//     return state.versus;
//   },
// } as GetterTree<State, any>;
