import {GetterTree, MutationTree, ActionTree, ActionContext} from 'Vuex';
import {CellData, GameData, XOXO, XOXOArray} from '@/types';
import {State} from './State';

export const actions = {
  async changeSymbol(store: ActionContext<State, any>) {
    await store.commit('setSymbol', !store.state.x);
  },
  // async countAdd(store: ActionContext<State, any>) {
  //   await store.commit('setCount', store.state.count + 1);
  // },
  async saveValue(store: ActionContext<State, any>, payload: CellData) {
    // console.log('action: ' + JSON.stringify(payload));
    if (
      !store.state.data[payload.position] ||
      store.state.data[payload.position] === XOXO.empty
    ) {
      await store.commit('setCellData', payload);
      await store.commit('setCount', store.state.count + 1);
    } else {
      throw new RangeError('field is not empty');
    }
  },
  async gameOver(store: ActionContext<State, any>) {
    store.commit('setFinish', true);
  },
  async newGame(store: ActionContext<State, any>, payload = {}) {
    // по сути в этом экшене мы выполняем роль конструктора
    // задавая значению всех полей, но в отличии от конструктора -
    // данную функцию мы можем вызывать когда угодно для того
    // чтобы обнулить данные игры

    const size = parseInt(payload.size, 10) || 3;
    const versus = (payload.type === 'versus');

    await store.commit('setFinish', false);
    await store.commit('setSymbol', true);
    await store.commit('setCount', 0);
    await store.commit('setSize', size);
    await store.commit('setVersus', versus);

    // Здесь правильно было бы использовать Promise.all для паралельного исполнения,
    // но так как это мгновенная операция в памяти - нет смысла портить синтаксис
    for (let key: number = 1, max: number = size * size; key <= max; key++) {
      await store.commit('setCellData', { position: key, value: XOXO.empty });
    }
  },
} as ActionTree<State, any>;
