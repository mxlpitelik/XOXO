import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { State } from './State';

export default {
  state: new State(),
  getters,
  mutations,
  actions,
};
