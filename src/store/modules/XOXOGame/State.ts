import {CellData, GameData, XOXO, XOXOArray} from '@/types';

export class State implements GameData {
  public size = 3;
  public x = true;
  public count = 0;
  public finish = false;
  public data = {} as XOXOArray;
  public versus = false;
}
