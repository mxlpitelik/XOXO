// import Vue from 'vue';
import {GetterTree, MutationTree, ActionTree, ActionContext} from 'Vuex';
import {CellData, GameData, XOXO, XOXOArray} from '@/types';
import { State } from './State';

export const mutations = {
  setSymbol(state: State, payload: boolean) {
    state.x = payload;
  },
  setSize(state: State, payload: number) {
    state.size = payload;
  },
  setCount(state: State, payload: number) {
    state.count = payload;
  },
  setCellData(state: State, payload: CellData) {
    state.data = { ...state.data, [payload.position]: payload.value };
    // Можно также использовать следующую функцию для добавления свойств
    // Vue.set(state.data, payload.position, payload.value);

    // Если же испольтзовать подобный метод, то свойства должны быть заранее инициализированы
    // state.data[payload.position] = payload.value;
  },
  // setGameData(state: State, payload: GameData) {
    // state = { ...state, ...payload };
    // state = payload;

    // const zero: State = new State();
    // работать не будет следующая конструкция
    // state = {...state, ...zero};

    // эта уже будет работать, но тайпскрипт будет ругаться
    // for (const key in zero) {
    //   Vue.set(state, key, zero[key]);
    // }
  // },
  setFinish(state: State, payload: boolean) {
    state.finish = payload;
  },
  setVersus(state: State, payload: boolean) {
    state.versus = payload;
  },
} as MutationTree<State>;
