import Vue from 'vue';
import Vuex from 'vuex';

import {CellData, GameData, XOXO, XOXOArray} from './types';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    game: {
      x: true,
      count: 0,
      finish: false,
      data: {},
    } as GameData,
  },

  mutations: {
    setSymbol(state, payload: boolean) {
      state.game.x = payload;
    },
    setCount(state, payload: number) {
      state.game.count = payload;
    },
    setCellData(state, payload: CellData) {
      alert('mutation: ' + payload.position);
      state.game.data[payload.position] = payload.value;
    },
    setGameData(state, payload: GameData) {
      state.game = payload;
    },
    setFinish(state, payload: boolean) {
      state.game.finish = payload;
    },
  },

  actions: {
    changeSymbol(ctx) {
      ctx.commit('setSymbol', !ctx.state.game.x);
    },
    countAdd(ctx) {
      ctx.commit('setCount', ctx.state.game.count + 1);
    },
    saveValue(ctx, payload: CellData) {
      alert('action: ' + JSON.stringify(payload));
      if (
        !ctx.state.game.data[payload.position] ||
        ctx.state.game.data[payload.position] === XOXO.empty
      ) {
        ctx.commit('setCellData', payload);
      } else {
        throw new RangeError('field is not empty');
      }
    },
    gameOver(ctx) {
      ctx.commit('setFinish', true);
    },
    resetGame(ctx) {
      const zero = {
        x: true,
        count: 0,
        finish: false,
        data: {},
      } as GameData;

      ctx.commit('setGameData', zero);
    },
  },
  getters: {
    getSymbol(state): XOXO {
      return (state.game.x) ? XOXO.x : XOXO.o;
    },
    getCount(state): number {
      return state.game.count;
    },
    getCellData: (state) => (position: number): XOXO => {
      return (state.game.data[position]) ? state.game.data[position] : XOXO.empty;
    },
    getGameData(state): XOXOArray {
      return state.game.data;
    },
    isGameOver(state): boolean {
      return state.game.finish;
    },
  },
});
