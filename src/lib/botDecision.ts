import { GameData, XOXOArray, XOXO } from '@/types';

export default (size: number, data: XOXOArray) => {

    const max = size * size + 1;

    let position: number = 0;
    while (!position) {
        const p: number = Math.trunc(Math.random() * (max - 1) + 1);
        if (data[p] === XOXO.empty) {
        position = p;
        }
    }

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(position);
        }, 2000);
    });
};

