import { XOXOArray, XOXO } from '@/types';

export default (data: XOXOArray, size: number): XOXO => {

    const max = size * size;

    // проверим горизонтальные линии
    // let prev
    for (let first = 1; first < max; first += size) {
       for (let next = first + 1, n = 2; n <= size; next++, n++) {
         if (data[first] === XOXO.empty || data[first] !== data[next]) {
           break;
         }
         if (n === size) {
           return data[first];
         }
       }
    }
    // проверим вертикальные линии
    for (let first = 1; first < max; first ++) {
       for (let next = first + size, n = 2; n <= size; next += size, n++) {
         if (data[first] === XOXO.empty || data[first] !== data[next]) {
           break;
         }
         if (n === size) {
           return data[first];
         }
       }
    }

    // проверим косые лево-верх - право-низ
    for (let first = 1, next = 1, step = size + 1; next <= max; next += step) {
      if (data[first] === XOXO.empty || data[first] !== data[next]) {
        break;
      }
      if (next === max) {
        return data[first];
      }
    }

    // проверим косую право-верх - лево-низ
    for (let first = size, next = size, step = size - 1, n = 1; n <= size; next += step, n++) {

      if (data[first] === XOXO.empty || data[first] !== data[next]) {
        break;
      }
      if (n === size) {
        return data[first];
      }
    }

    return XOXO.empty;
};
